cmake_minimum_required(VERSION 3.16)

find_package(Qt6 REQUIRED COMPONENTS Widgets Quick QuickWidgets)

# set(CMAKE_CXX_FLAGS "-fsanitize=address")

add_executable(qwtest main.cpp)

target_link_libraries(qwtest PRIVATE Qt::Quick Qt::Widgets Qt::QuickWidgets)
