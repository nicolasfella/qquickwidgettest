#include <QDialog>
#include <QQuickWidget>
#include <QApplication>
#include <QMainWindow>
#include <QPushButton>
#include <QLayout>

int main(int argc, char**argv){

    QApplication app(argc, argv);

    QMainWindow win;

    auto button = new QPushButton("Push me baby one more time");

    win.layout()->addWidget(button);

    QObject::connect(button, &QPushButton::clicked, [&win]{
        QDialog *dlg = new QDialog(&win);

        QWidget *w = new QWidget(dlg);

        QQuickWidget *qw = new QQuickWidget(w);
        dlg->show();

    });

    win.show();
    return app.exec();
}
